<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {


	const TABLE = "seg_products";
	function __construct() {
		parent::__construct();
		$this->load->database();
	}


	/**
	 * Method to register new produt
	 * Validations of fields and code of the product.
	 * @code: Value unique of the product.
	 *
	 * @return bool
	 * @author: vla200632213@gmail.com(Luis Vega)
	 **/
	public function save($data) {
		$resulSet = $this->db->get_where(self::TABLE, ['prod_code' => $data['code']]);
		$product = $resulSet->result();
		if (!empty($product))
			return false;

		$this->db->insert(self::TABLE, $data);
		return true;
	}

	/**
	 * Method to find all products by $filters.
	 * @code: Value int unique assigned to product.
	 *
	 * @return array
	 *
	 * **/
	public function findAllBy($filters) {

		if (!empty($filters['code'])) {
			$this->db->like('prod_code', $filters['code']);
		}
		//Per Enabled(Not remove) products
		$this->db->where('prod_enable', true);

		return $this->db->get(self::TABLE)->result();

	}

	/**
	 * Method to find one product by $code.
	 * @code: Value int unique assigned to product.
	 *
	 * @return object
	 * **/
	public function findBy($code) {

		$queryFilters = [];
		if (!empty($code)) {
			$this->db->where('prod_enable', true);
			$resulSet = $this->db->get_where(self::TABLE, ['prod_code' => $code]);
			if (!empty($resulSet))
				return $resulSet;
		}
		return false;

	}

	public function update($userId, $userData) {
		$resulSet = $this->db->get_where('seg_persona', ['username' => $userData['username'], 'pers_id !=' => $userId]);
		$person = $resulSet->result();
		if (!empty($person))
			return false;

		if (!empty($userData['password'])) {
			$userData['password'] = md5($userData['password'] . 'SEG');
		} else {
			unset($userData['password']);
		}
		$this->db->where('idpers_id', $userId);
		$this->db->update('seg_persona', $userData);

		return true;
	}
}
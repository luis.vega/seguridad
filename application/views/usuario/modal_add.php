<div class="modal fade" id="modal-add-product" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">New Product </h3>
		</div>
		<div class="modal-body">

            <!-- content goes here -->
			<form id="form-product">
              <div class="form-group">
                <label for="nombres">ID</label>
                <input type="text" class="form-control" id="code" placeholder="Item ID" name="code" required>
              </div>
              <div class="form-group">
                <label for="apellidos">Title</label>
                <input type="text" class="form-control" id="title" placeholder="Title" name="title" required>
              </div>
              <div class="form-group">
                <label for="username">Seller Name</label>
                <input type="text" class="form-control" id="seller_name" placeholder="Seller Name" name="seller_name" required>
              </div>
              <div class="form-group">
                <label for="price">Price</label>
                <input type="numeric" class="form-control" id="price" placeholder="Price" name="price" required>
              </div>

              <button type="submit" id="save-product" class="btn btn-success btn-hover-green" data-action="save" role="button">Register</button>
            </form>

		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
				</div>
				<div class="btn-group btn-delete hidden" role="group">
					<button type="button" id="delImage" class="btn btn-default btn-hover-red" data-dismiss="modal"  role="button" id="close-modal">Delete</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
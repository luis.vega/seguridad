<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

?>

	<p class="footer">Copyright © <strong>ALL-TIQ</strong> - <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>

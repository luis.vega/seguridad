<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Seguridad</title>

	<link rel="stylesheet" type="text/css" href="public/assets/css/bootstrap.min.css" media="screen" />
	<script type="text/javascript" src="public/assets/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="public/assets/js/bootstrap.min.js"></script>
</head>
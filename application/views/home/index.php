<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
	$this->load->view('home/header');
 ?>
<body>

	<script type="text/javascript" src="<?= base_url(); ?>assets/js/product/index.js"></script>

<?php
	$this->load->view('usuario/modal_add');
 ?>
 <script type="text/javascript">
 	var baseUrl = "<?= base_url(); ?>";
 </script>
	<div id="container">
		<div class="col-md-1"></div>
		<div id="body" class="col-md-10">
			<h1>List Products</h1>

			<div class="col-md-12">
				<div class="form-group">
					<input type="button" id="add-persona" value=" + PRODUCT" class="btn btn-primary"
					data-toggle="modal" data-target="#modal-add-product">
				</div>
			</div>
			<table class="table table-striped">
				<tbody>
					<tr>
						<th width="10%">Item ID</th>
						<th>Title</th>
						<th width="20%">Seller</th>
						<th width="10%">Price</th>
						<th width="10%">Actions</th>
					</tr>
				</tbody>
				<tbody>
					<?php if(!empty($collection) && sizeof($collection) > 0) : ?>
						<?php
							foreach ($collection as $product) :
								$product = (object) $product;
						?>
							<tr>
								<td>
									<?= $product->prod_code;?>
								</td>
								<td>
									<?php echo $product->prod_title; ?>
								</td>
								<td>
									<?php echo $product->prod_seller_name; ?>
								</td>
								<td>
									<?php echo $product->prod_price; ?>
								</td>
								<td class="text-center">
									<a class='btn btn-info btn-xs' href="#" title="Click by Update Product"><span class="glyphicon glyphicon-edit"></span></a>
									<a href="#" class="btn btn-danger btn-xs" title="Click by Remove Product" ><span class="glyphicon glyphicon-remove"></span></a>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td colspan="4" class="text-center">
								<strong>No records</strong>
							</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
		<div class="col-md-1"></div>

	</div>
	<footer class="col-md-12 text-center">
	<?php
		$this->load->view('home/footer');
	 ?>
	</footer>

</body>
</html>
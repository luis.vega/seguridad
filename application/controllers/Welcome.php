<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('product_model');
	}



	/**
	 * Index Page principal the show list of proudcts.
	 *
	 */
	public function index()
	{
		$filterCode = $this->input->get('id');

		$filters = [];
		if (!empty($filterCode)) {
			$filters['code'] = trim($filterCode);
		}
		$this->load->helper('url');
		$products = $this->product_model->findAllBy($filters);
		$data = ['collection' => $products];
		$this->load->view('home/index', $data);

	}
}

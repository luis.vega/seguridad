<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiService extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('product_model');
	}

	public function save_product() {
		$data = [
			'prod_title' => $this->input->post('title'),
			'prod_seller_name' => $this->input->post('seller_name'),
			'prod_code' => $this->input->post('code'),
			'prod_price' => round($this->input->post('price'), 2)
		];
		$response = [];
		$typeResponse = 200;

		if (empty($data['title']) || empty($data['seller_name'])
			|| empty($data['code']) || empty($data['price'])) {
			$response['error'] = "Exiten parametros requeridos sin valor";
			$typeResponse = 403;
		} else {
			$responseSave = $this->product_model->save($data);
			if (!empty($responseSave)) {
				$response['message'] = 'Su registro fue procesado';
			} else {
				$response['error'] = "No se logro registrar";
				$typeResponse = 403;
			}
		}

		$this->output
	            ->set_content_type('application/json')
	            ->set_status_header($typeResponse);
		echo json_encode($response);
	}

}
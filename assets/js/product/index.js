$(document).ready(function() {

	// Limpiar el modal
	$(".modal").on("hidden.bs.modal", function(){
		$('input' ,'form#form-product').val('');
	});

	$('#form-produt').submit(function(e) {
		e.preventDefault();
		var id = parseInt($('#code').val());
		if (id && id > 0 && $('#code').val().length < 9) {
			var data = $("#form-product :input").serialize();
			saveProduct(data);
		} else {
			alert('Item Id is numeric and max size 9 digits.');
		}

	});
});

function saveProduct(data) {
		$.ajax({
		url: baseUrl + "apiservice/save_product",
		method: "POST",
		data: data,
		dataType : 'json',
		success: function(result){
			if (result && result.message) {
    			alert(result.message);
    			//location.reload(true);
			}
    	},
    	error: function(datosError) {
    		if (datosError && datosError.responseJSON)
    			alert(datosError.responseJSON.error);
    		else
    			alert('Fatal Error contact to support technical.');
    	}
    });
}
-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.18-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db.eke.seg_products
CREATE TABLE IF NOT EXISTS `seg_products` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_title` varchar(200) NOT NULL,
  `prod_seller_name` varchar(250) NOT NULL,
  `prod_code` varchar(9) NOT NULL,
  `prod_enable` tinyint(1) NOT NULL DEFAULT '1',
  `prod_price` float NOT NULL,
  PRIMARY KEY (`prod_id`),
  UNIQUE KEY `username` (`prod_code`),
  KEY `pers_id` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db.eke.seg_products: ~0 rows (approximately)
/*!40000 ALTER TABLE `seg_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `seg_products` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
